from email.policy import default
from odoo import api, fields, models
import string


class customerType(models.Model):
    _name = 'nrs.customer.type'
    _description = 'Configuration to determine the type of the Customer'
    _rec_name = 'customer_type_name'

    customer_type_name = fields.Char(string='Customer Type Name')
    customer_type_mode = fields.Selection(string='Customer Type Mode', selection=[('generate', 'Generate Code'), ('single', 'Single Code'), ('manual', 'Manual Code')], default='generate')
    customer_code = fields.Many2one('ir.sequence', readonly=True, copy=False, compute="_generate_cust_code")
    customer_code_str = fields.Char(string='Customer Code')
    next_number = fields.Integer(string='Next Number', related="customer_code.number_next")
    
    @api.depends('customer_type_name','customer_type_mode')
    def _generate_cust_code(self):
        for record in self:
            # record.customer_code = False
            if record.customer_type_name and record.customer_type_mode == 'generate':
                # if record.customer_type_mode == 'generate':
                master_sequence = self.env['ir.sequence'].search([('name', 'like', record.customer_type_name)])
                if not master_sequence:
                    vals = {
                            'name': '%s - Sequence'% (record.customer_type_name),
                            'code': 'nrs.customer.type.sequence',
                            'implementation': 'no_gap',
                            'prefix': 'MTR/',
                            'suffix': '',
                            'number_next': 1,
                            'number_increment': 1,
                            'padding': 6,
                            'company_id': 1}
                    master_sequence = self.env['ir.sequence'].create(vals)
                record.customer_code = master_sequence.id
                record.customer_code_str = master_sequence.prefix.translate(str.maketrans('', '', string.punctuation))
            else:
                record.customer_code = False
    @api.model
    def create(self, values):
        # Add code here
        customer_type_id = super(customerType, self).create(values)
        # customer_type = values.get('customer_type')
        # partner_ids = self.env['res.partner'].search([('cust_type_id', '=', customer_type_id.id)])
        # if customer_type == 'generate':
        #     master_sequence = self.env['ir.sequence'].search([('name', 'like', customer_type_id.customer_type_name)])
        #     master_sequence.write({'number_next': 1})
        #     for partner in partner_ids:
        #         cust_code = master_sequence.next_by_id()
        #         partner.write({
        #             'cust_code': cust_code,
        #             'prev_code': cust_code
        #             })
        # elif customer_type == 'single':
        #     for partner in partner_ids:
        #         partner.write({'cust_code': customer_type_id.customer_code_str})
        return customer_type_id
    
    def write(self, values):
        # Add code here
        customer_type = values.get('customer_type')
        partner_ids = self.env['res.partner'].search([('cust_type_id', '=', self.id)])
        if customer_type:
            master_sequence = self.env['ir.sequence'].search([('name', 'like', self.customer_type_name)])
            master_sequence.write({'number_next': 1})
            if customer_type == 'generate':
                for partner in partner_ids:
                    cust_code = master_sequence.next_by_id()
                    partner.write({
                        'cust_code': cust_code,
                        'prev_code': cust_code
                        })
            elif customer_type == 'single':
                for partner in partner_ids:
                    partner.write({'cust_code': self.customer_code_str})
        return super(customerType, self).write(values)   


class custType(models.Model):
    _inherit = 'res.partner'

    cust_type_id = fields.Many2one(comodel_name='nrs.customer.type', string='Customer Type', ondelete='restrict', tracking=True)
    cust_code = fields.Char(string='Customer Code', compute="_generate_cust_code", store=True, tracking=True)
    prev_code = fields.Char(string='Previous Code')
    customer_type_mode = fields.Selection(string='Customer Type Mode', selection=[('generate', 'Generate Code'), ('single', 'Single Code'), ('manual', 'Manual Code')], related='cust_type_id.customer_type_mode')
    
    @api.depends('cust_type_id')
    def _generate_cust_code(self):
        for record in self:
            master_sequence = self.env['ir.sequence'].search([('name', 'like', record.cust_type_id.customer_type_name)])
            if record.cust_type_id:
                if record.cust_type_id.customer_type_mode == 'generate':
                    if not record.prev_code:
                        cust_code = master_sequence.next_by_id()
                        record.prev_code = cust_code
                        record.cust_code = cust_code
                    elif record.prev_code:
                        record.cust_code = record.prev_code
                elif record.cust_type_id.customer_type_mode == 'single':
                    record.cust_code = record.cust_type_id.customer_code_str
                else:
                    record.cust_code = False
            else:
                record.cust_code = False
    