{
    "name"          : "Nurosoft - Realfood - Kode Customer",
    "version"       : "1.0",
    "author"        : "@alvins & nurosoft Team",
    "category"      : "Sales/Sales",
    "description"   : "",
    ## butuh depends ke sl_task karena membutuhkan project.mastertask
    "depends"       : ['sale_management'],
    "data"          : [ 
                        'security/ir.model.access.csv',
                        'views/nrs_customer_type_views.xml',
                    ],
    'installable'   : True,
    'active'        : False,
    'application'   : True,
}